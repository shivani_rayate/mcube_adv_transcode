
const mongoose = require('mongoose');



const imdbSchema = mongoose.Schema({
    asset_id: { type: String },
    metadata: { type: Object },
});


imdbSchema.pre('save', function (next) {

    if (this.isNew) {

        console.log('IS NEW CALLED imdbSchema!!');
        this.created_at = new Date();
        this.updated_at = new Date();
        next();
    }else { 
        console.log('IS NEW IS FALSE imdbSchema!!');
        this.updated_at = new Date();
        this.updated_by = this.user_id;
        next();
    }
});




imdbSchema.post('save', function (doc) {

});


module.exports = mongoose.model('ImdbData', imdbSchema, 'ImdbData');