var jwt = require('jsonwebtoken');
var request = require('request');
var jwkToPem = require('jwk-to-pem');

module.exports = (req, res, next) => {
    try {
        console.log('auth middleware called >>');
        var token = req.headers.token;
        var decodedJwt = jwt.decode(token, { complete: true });

        if (!decodedJwt) {
            console.log("Not a valid JWT token");
            return res.json({
                status: 400,
                message: 'Not a valid JWT token !!!'
            })
        } else {
            var kid = decodedJwt.header.kid;
            var iss = decodedJwt.payload.iss;
            //Download the JWKs and save it as PEM
            request({
                url: `${iss}/.well-known/jwks.json`,
                json: true
            }, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    var pems = {};
                    var keys = body['keys'];

                    for (var i = 0; i < keys.length; i++) {
                        //Convert each key to PEM
                        key_id = keys[i].kid;
                        modulus = keys[i].n;
                        exponent = keys[i].e;
                        key_type = keys[i].kty;
                        jwk = { kty: key_type, n: modulus, e: exponent };
                        pem = jwkToPem(jwk);
                        pems[key_id] = pem;
                    }

                    var _pem = pems[kid];

                    if (!_pem) {
                        console.log('Invalid access token');
                        return;
                    }
                    jwt.verify(token, _pem, { issuer: iss }, function (err, payload) {
                        if (err) {
                            console.log("Unauthorized access token");

                            return res.json({
                                status: 400,
                                message: 'Not a valid JWT token !!!'
                            })

                        } else {

                            // get tenantId from the decoded token payload 
                            res.locals.tenantId = iss.slice(iss.lastIndexOf('/') + 1);
                            next()
                        }

                    })

                } else {
                    return res.json({
                        status: 400,
                        message: 'Not a valid JWT token !!!'
                    })
                }
            });

        }

    } catch (err) {
        return res.status(401).json({
            status: 401,
            message: 'Auth Failed in cognito !!!'
        })
    }
}