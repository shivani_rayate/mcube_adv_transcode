const express = require('express');
const router = express.Router();
const AWS = require('aws-sdk');

var fs = require("fs");

const tenantConfig = require('../../config/tenantConfig');


// add tenant config to config.json file
router.post('/addTenantConfig', (req, res, next) => {

    console.log("addTenantConfig called in API");

    var config = req.body.config;
    var tenantName = req.body.config.poolName;
    var tenantId = req.body.config.poolId;

    tenantConfig[tenantId] = { tenantName: tenantName, config };

    fs.writeFile('config/tenantConfig.json', JSON.stringify(tenantConfig, null, 2), (err) => {
        if (err) {
            console.error(err);
            return;
        } else {
            res.json({
                status: 200,
                message: `Tenant config udated in mcube UI`,
                data: null
            });
        }

    });


})


// delete tenant config from config.json file
router.post('/deleteTenantConfig', (req, res, next) => {
    console.log("deleteTenantConfig called in API");

    var params = {
        userPoolId: req.body.userPoolId ? req.body.userPoolId : null,
        poolName: req.body.poolName ? req.body.poolName : null

    }

    delete tenantConfig[params.userPoolId];
    fs.writeFile('config/tenantConfig.json', JSON.stringify(tenantConfig, null, 2), (err) => {
        if (err) {
            console.error(err);
            return;
        } else {
            res.json({
                status: 200,
                message: `Tenant config deleted in mcubeUI`,
                data: null
            });
        }

    });
})



module.exports = router;