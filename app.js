const express = require('express');
const app = express();
const router = express.Router();
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const compression = require('compression');
const chalk = require('chalk');
const helmet = require('helmet');
const cors = require('cors');

const mongoDb = require('./config/mongoDb');

// connect to mongoDB
mongoDb.getMongoDbs().then((dbs) => {
  for (let db of dbs) {
    mongoDb.connectMongoDb(db.name)
  }
})


// req for middleware
var check_auth = require('./api/middleware/check-auth')

// Routes Which should Handle Request
const video = require('./api/routes/video');
const config = require('./api/routes/config');

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3002);


app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
  limit: '26000mb',
  extended: true,
}));
app.use(bodyParser.json({ limit: '26000mb' }));

app.use(logger('dev'));

app.use('/api', router);
app.use('/config', config);

app.use('/api/video', check_auth, video);



app.use(cookieParser());


// helmet for security purpose
app.use(helmet());

// CORS - To hanlde cross origin requests
app.use(cors());

//compress all responses to Make App Faster
app.use(compression());

/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
  console.log('  Press CTRL-C to stop\n');
});



module.exports = app;
